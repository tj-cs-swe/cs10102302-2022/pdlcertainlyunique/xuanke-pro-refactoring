const {defineConfig} = require('@vue/cli-service');
const path = require('path');
function resolve (dir) {
  return path.join(__dirname, dir);
}
module.exports = defineConfig({
  transpileDependencies: true,
  chainWebpack (config) {
    // set svg-sprite-loader
    config.module
      .rule('svg')
      .exclude.add(resolve('src/assets/icons'))
      .end();
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/assets/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end();
  },
  devServer:{
    proxy:{
      '/api':{
        // 本地服务器向 target服务器请求数据
        target:'http://localhost:3000/',
        secure: false,  // 如果是https接口,需要配置这个参数
        // 允许跨域
        changeOrigin:true,
      }
    }
  }
});
