const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');

const config = require('./config/config.js');
const apiRouter = require('./routes/api');
const mongoose = require('mongoose');

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// 配置session
require('./session.js')(app);

// 连接数据库
mongoose.connect(`mongodb://${config.db.host}:${config.db.port}/${config.db.database}`);
let db = mongoose.connection;
db.on('error', () => {
  console.error('mongodb: 连接错误');
});

// 设置跨域访问
app.use(cors({
  origin: `${config.cors.origin}`,
  credentials: true,
}));

app.use('/images',express.static('images'));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', apiRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;