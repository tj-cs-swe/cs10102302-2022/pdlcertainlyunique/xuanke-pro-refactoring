module.exports = (Schema, model) => {
  const emailSchema = new Schema({
    email: String,
    code: String,
    time: Date,
  });

  const Emails = model('Emails', emailSchema);

  return Emails;
};
