module.exports = (Schema, model) => {
  const courseSchema = new Schema({
    _id: String,
    name: String,
    teacher: String,
    tid: String,
    place: String,
    type: String,
    language: String,
    department: String,
    length: Number,
    period: Number,
    day: Number,
    room: String,
  });

  const Courses = model('Courses', courseSchema);
  
  return Courses;
};