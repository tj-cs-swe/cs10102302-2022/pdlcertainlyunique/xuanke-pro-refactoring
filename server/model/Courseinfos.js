module.exports = (Schema, model) => {
  const courseinfoSchema = new Schema({
    course_id: String,
    msg: String,
    courseTitle: String,
    courseTime: String,
    credit: String,
    teacher: String,
    assessment: String,
    teacher_message: String,
    course_intro: String,
    outline: Array,
    labels: Array,
  });

  const Courseinfos = model('Courseinfos', courseinfoSchema);
  
  return Courseinfos;
};