module.exports = (Schema, model) => {
  const questionSchema = new Schema({
    course_id: String,
    head: String,
    content: String,
    thumbnail: String,
    user_id: String,
    timestamp: Number,
    reply_stamp: Number,
    reply_cnt: Number,
    liked: Array,
    sticked: Boolean,
    image_list: Array,
  });

  const Questions = model('Questions', questionSchema);

  return Questions;
};