module.exports = (Schema, model) => {
  const userSchema = new Schema({
    account: String,
    password: String,
    name: String,
    role: String,
    gender: String,
    year: String,
    date: String,
    grade: String,
    region: String,
    email: String,
    desc: String,
    courses: Array,
    favorites_courses: Array,
    avatar: String
  });

  const Users = model('Users', userSchema);

  return Users;
};