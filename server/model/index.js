const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const model = mongoose.model.bind(mongoose);
const fs = require('fs');
const path = require('path');

const db = {};

fs
  .readdirSync(__dirname)
  .filter((file) =>
    file !== 'index.js'
  ) // 读取 model 文件夹下的模型文件，文件名应位复数形式
  .forEach((file) => {
    const modelFunction = require(path.join(__dirname, file));
    const _model = modelFunction(Schema, model); // 调用函数生成模型
    db[file.split('.')[0]] = _model; // save
  });

module.exports = db;