module.exports = (Schema, model) => {
  const replySchema = new Schema({
    question_id: String,
    reply_content: String,
    user_id: String,
    reply_stamp: Number,
    liked: Array,
    image_list: Array,
  });

  const Replies = model('Replies', replySchema);

  return Replies;
};