module.exports = (Schema, model) => {
  const commentsSchema = new Schema({
    course_id: String,
    score: Number,
    user_id: String,
    content: String,
    time: Number,
    liked: Array,
    like_count: Number,
    pic: String,
  });
  
  const Comments = model('Comments', commentsSchema);

  return Comments;
};