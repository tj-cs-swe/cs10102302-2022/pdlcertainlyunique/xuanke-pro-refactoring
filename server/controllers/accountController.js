const Model = require('../model');
const { Users } = Model;
const tokenApi = require('../token.js');

const AccountController = {
  // 添加用户
  async add(req, res) {
    const newUser = new Users(req.body);
    try {
      const user = await Users.findOne({ email: newUser.email }).exec();
      if (user) {
        res.json({
          code: 400,
          msg: '用户已存在',
        });
      }
      else {
        try {
          const user = await newUser.save();
          Users.findOne({ _id: user._id }, (err, user) => {
            res.json({
              code: 200,
              msg: '添加成功',
              user: user,
            });
          });
        } catch (err) {
          res.json({
            code: 500,
            msg: '服务器错误',
          });
        }
      }
    } catch (err) {
      res.status(500).json({
        code: 500,
        msg: '服务器错误',
      });
    }
  },
  // 登录验证
  async login(req, res) {
    let { email, password } = req.query;
    try {
      const user = await Users.findOne({ email }).exec();
      if (!user) {
        res.json({
          code: 404,
          msg: '用户不存在',
        });
      }
      else if (user.password !== password) {
        res.json({
          code: 400,
          msg: '密码不正确',
        });
      }
      else {
        tokenApi.setToken(email).then(token => {
          res.json({
            code: 200,
            msg: '登录成功',
            username: user.name,
            role: user.role,
            avatar: user.avatar,
            email: user.email,
            token: token,
          });
        });
      }
    } catch (err) {
      res.status(500).json({
        code: 500,
        msg: '服务器错误',
      });
    }
  },
  // 重置密码
  async resetPassword(req, res) {

    let { email, password } = req.body;
    try {
      const user = await Users.findOne({ email }).exec();
      if (!user) {
        res.json({
          code: 404,
          msg: '账号不存在',
        });
      }
      else {
        try {
          await Users.updateOne({ email }, { password }).exec();
          res.json({
            code: 200,
            msg: '修改成功',
          });
        } catch (err) {
          res.status(500).json({
            code: 500,
            msg: '服务器错误',
          });
        }
      }
    } catch (err) {
      res.status(500).json({
        code: 500,
        msg: '服务器错误',
      });
    }
  },
  // 修改密码
  async changePassword(req, res) {
    let { email, password, new_password } = req.body;
    try {
      const user = await Users.findOne({ email }).exec();
      if (!user) {
        res.json({
          code: 404,
          msg: '账号不存在',
        });
      }
      else if (user.password !== password) {
        res.json({
          code: 400,
          msg: '原密码不正确',
        });
      }
      else {
        try {
          await Users.updateOne({ email }, { password: new_password }).exec();
          res.json({
            code: 200,
            msg: '修改成功',
          });
        } catch (err) {
          res.status(500).json({
            code: 500,
            msg: '服务器错误',
          });
        }
      }
    } catch (err) {
      res.status(500).json({
        code: 500,
        msg: '服务器错误',
      });
    }
  }
};  

module.exports = AccountController;