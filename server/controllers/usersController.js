const Model = require('../model');
const { Users, Courses } = Model;

async function findOneUserByEmailPsw (_email, _password) {
  const user = await Users.findOne(
    {
      email: _email
    },
    {
      password: _password
    }
  ).exec();
  return user;
}

const usersController = {
  // 查询所有用户
  async all (req, res) {
    try {
      const users = await Users.find({}).exec();
      res.json({
        code: 200,
        msg: '查询成功',
        data: users,
      });
    } catch (err) {
      res.json({
        code: 500,
        msg: '服务器错误',
      });
    }
  },
  // 获取账号个人信息
  async getUserinfo(req, res) {
    if (!req.query['email']) {
      return res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: '请发送对应的账号email参数',
      });
    }
    // query 中存在邮箱，则查询数据库获取相应用户
    try {
      const user = await findOneUserByEmailPsw(req.query['email'], 0);

      if (!user) {
        return res.json({
          code: 400,
          msg: '查询失败',
          err: '没有找到该用户',
        });
      }

      res.json({
        code: 200,
        msg: '查询成功',
        data: user,
      });
    } catch (err) {
      res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: err,
      });
    }
  },
  // 添加账号个人信息
  async addUserinfo (req, res) {
    if (!req.body['email']) {
      return res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: '请发送对应的账号email参数',
      });
    }
    try {
      // 查询对应用户
      const user = await Users.findOne({ 'email': req.body['email'] }).exec();
      // 更新 user
      await Users.findByIdAndUpdate(
        user._id,
        {
          $set: {
            name: req.body['name'],
            year: req.body['year'],
            date: req.body['date'],
            region: req.body['region'],
            desc: req.body['desc'],
            grade: req.body['grade'],
          }
        }
      ).exec();

      res.json({
        code: 200,
        msg: '添加成功'
      });
    } catch (err) {
      res.status(400).json({
        code: 400,
        msg: '修改失败',
        err: err,
      });
    }
  },
  // 请求课表，邮箱版
  async getTimetableByEmail (req, res) {
    if (!req.query['email']) {
      return res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: '请发送对应的email参数',
      });
    }
    try {
      // 根据 email 获取对应用户
      const user = await findOneUserByEmailPsw(req.query['email'], 0);

      if (!user) {
        return res.json({
          code: 400,
          msg: '查询失败',
          err: '没有找到该用户'
        });
      }

      const courses = await Courses.find({
        _id: {
          $in: user['courses']
        }
      }).exec();

      res.json({
        code: 200,
        msg: '查询成功',
        num: courses.length,
        courses: courses,
      });
    } catch (err) {
      res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: err,
      });
    }
  },
  // 添加课表
  async addTimetable (req, res) {
    if (!req.body['email']) {
      return res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: '请发送对应的email参数',
      });
    }
    try {
      const user = await Users.findOne({ 'email': req.body['email'] }).exec();
      await Users.findByIdAndUpdate(
        user._id,
        {
          $addToSet : {
            courses: req.body['cid']
          },
        }
      ).exec();
      res.json({
        code: 200,
        msg: '添加成功'
      });
    } catch (err) {
      res.status(400).json({
        code: 400,
        msg: '修改失败',
        err: err,
      });
    }
  },
  // 删除课表
  async deleteTimetable (req, res) {
    if (!req.body['email']) {
      return res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: '请发送对应的email参数',
      });
    }
    try {
      const user = await Users.findOne({ 'email': req.body['email'] }).exec();
      await Users.findByIdAndUpdate(
        user._id,
        {
          $pull: {
            courses: req.body['cid']
          }
        }
      ).exec();
      res.json({
        code: 200,
        msg: '删除成功'
      });
    } catch (err) {
      res.status(400).json({
        code: 400,
        msg: '修改失败',
        err: err,
      });
    }
  },
  // 添加用户收藏课程
  async addCollection (req, res) {
    if (!req.body['email']) {
      return res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: '请发送对应的email参数',
      });
    }
    try {
      const user = await Users.findOne({ 'email': req.body['email'] }).exec();
      await Users.findByIdAndUpdate(
        user._id,
        {
          $addToSet: {
            favorites_courses: req.body['cid']
          },
        }
      ).exec();
      res.json({
        code: 200,
        msg: '添加成功'
      });
    } catch (err) {
      res.status(400).json({
        code: 400,
        msg: '修改失败',
        err: err,
      });
    }
  },
  // 删除用户收藏课程
  async deleteCollection (req, res) {
    if (!req.body['email']) {
      return res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: '请发送对应的email参数',
      });
    }
    try {
      const user = await Users.findOne({ 'email': req.body['email'] }).exec();
      await Users.findByIdAndUpdate(
        user._id,
        {
          $pull: {
            favorites_courses: req.body['cid']
          },
        }
      ).exec();
      res.json({
        code: 200,
        msg: '删除成功'
      });
    } catch (err) {
      res.status(400).json({
        code: 400,
        msg: '修改失败',
        err: err,
      });
    }
  },
  // 添加用户头像链接
  async addAvatar (req, res) {
    if (!req.body['email']) {
      return res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: '请发送对应的email参数',
      });
    }
    try {
      const user = await Users.findOne({ 'email': req.body['email'] }).exec();
      
      await Users.findByIdAndUpdate(
        user._id,
        {
          $set: {
            avatar: req.body['avatar']
          },
        }
      ).exec();

      res.json({
        code: 200,
        msg: '添加成功'
      });
    } catch (err) {
      res.status(400).json({
        code: 400,
        msg: '修改失败',
        err: err,
      });
    }
  },
  // 获取用户收藏课程
  async getCollection (req, res) {
    if (!req.query['email']) {
      return res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: '请发送对应的email参数',
      });
    }
    try {
      const user = await Users.findOne({ email: req.query['email'] }).exec();

      if (!user) {
        return res.json({
          code: 400,
          msg: '查询失败',
          err: '没有找到该用户'
        });
      }

      const course = await Courses.find({
        _id: {
          $in: user['favorites_courses']
        }
      }).exec();
      res.json({
        code: 200,
        msg: '查询成功',
        num: course.length,
        collections: course,
      });
    } catch (err) {
      res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: err,
      });
    }
  },
};

module.exports = usersController;