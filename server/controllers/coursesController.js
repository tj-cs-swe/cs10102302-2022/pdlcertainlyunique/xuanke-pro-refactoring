const { query } = require('express');
const Model = require('../model');
const { Courses } = Model;

const coursesController = {
  // 获取课程
  async getCourses(req, res) {
    try{
      const coursesQuery = Courses.find({});
      let andall = [];
      var searchList = ['_id','name','teacher','tid','place','type','language','department','room'];
      for(var searchItem of searchList){
        //按课程ID号查找
        if (req.query[searchItem]) {
          let regs = [];
          if (Array.isArray(req.query[searchItem])) {
            req.query[searchItem].forEach((item) => {
              let tmpMap={};
              tmpMap[searchItem]={
                $regex: item,
                $options: 'i'
              };
              regs.push(tmpMap);
            });
            andall.push({
              $or: regs
            });
          }
          else {
            regs = new RegExp(req.query[searchItem], 'i');
            coursesQuery.where(searchItem).regex(regs);
          }
        }
      }
      // //按课程ID号查找
      // if (req.query['_id']) {
      //   let regs = [];
      //   if (Array.isArray(req.query['_id'])) {
      //     req.query['_id'].forEach((item) => {
      //       regs.push({
      //         _id: {
      //           $regex: item,
      //           $options: 'i'
      //         }
      //       });
      //     });
      //     andall.push({
      //       $or: regs
      //     });
      //   }
      //   else {
      //     regs = new RegExp(req.query['_id'], 'i');
      //     mquery.where('_id').regex(regs);
      //   }
      // }
      
      if (req.query['day_period']) {
        let regs = [];
        test = req.query['day_period'];
        req.query['day_period'].forEach((item) => {
          tmp = item.replace('[', '').replace(']', '').replaceAll('\\', '').replaceAll('"', '').split(',');
          tmp = tmp.map(Number);
          let day = Number(tmp[0]);
          let period = Number(tmp[1]);
          test = period;
          if (period) {
            regs.push({
              day: day,
              period: period
            });
          } else {
            regs.push({
              day: day
            });
          }
        });
        andall.push({
          $or: regs
        });
      }
      if (andall.length > 0) {
        coursesQuery.and(andall);
      }

      const courses = await coursesQuery.exec();
      
      res.json({
        code: 200,
        num: courses.length,
        courses: courses,
      });
        
    }catch(err){
      res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: err,
      });
    }

  } ,

};

module.exports = coursesController;
