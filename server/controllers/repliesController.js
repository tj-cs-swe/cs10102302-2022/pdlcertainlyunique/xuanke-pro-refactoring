const {
  Replies,
  Questions
} = require('../model');

const fs = require('fs');

async function findQuestion (userId, questionId) {
  const question = await Questions.findOne({
    _id: questionId,
  }).exec();
  
  let likeState = false;
  question['liked'].forEach(_userId => {
    if (userId === _userId) {
      likeState = true;
    }
  });

  const _question = {
    question_id: question['_id'],
    question_head: question['head'],
    question_content: question['content'],
    user_id: question['user_id'],
    reply_timestamp: question['reply_stamp'],
    reply_count: question['reply_cnt'],
    like_count: question['liked'].length,
    like_state: likeState,
    stick_state: question['sticked'],
    image_list: question['image_list'],
  };

  return _question;
}

const repliesController = {
  // 获取回答
  async getReply (req, res) {
    try {
      // 获取问题
      const question = await findQuestion(req.query['user_id'], req.query['question_id']);

      // 查询问题所对应的回复
      const replieQuery = Replies.find({
        question_id: req.query['question_id']
      });

      // 按回复时间降序排序
      replieQuery.sort({ reply_stamp: -1 });

      // 获取总回复数
      const counter = await replieQuery.clone().count();

      // 切片，获取需要显示的回复
      replieQuery.skip(req.query['idx_up']);
      replieQuery.limit(req.query['idx_low'] - req.query['idx_up']);

      // 执行查询
      const replies = await replieQuery.exec();

      const replyList = [];
      replies.forEach(reply => {
        // 判断回复是否被该用户 like
        let replyLiked = false;
        reply['liked'].forEach(userId => {
          if (req.query['user_id'] === userId) {
            replyLiked = true;
          }
        });
        // 保存需要返回的回复信息
        replyList.push({
          reply_id: reply['_id'],
          reply_content: reply['reply_content'],
          user_id: reply['user_id'],
          reply_timestamp: reply['reply_stamp'],
          like_count: reply['liked'].length,
          like_state: replyLiked,
        });
      });

      res.json({
        code: 200,
        msg: '查询成功',
        question: question,
        reply_count: counter,
        list_len: replyList.length,
        reply_list: replyList,
      });
    } catch (err) {
      console.log(err);
      res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: err,
      });
    }
  },
  // 添加回答
  async addReply (req, res) {
    try {
      const timeNow = Date.parse(new Date());

      // 保存新的回复
      const reply = new Replies({
        question_id: req.body['question_id'],
        reply_content: req.body['reply_content'],
        user_id: req.body['user_id'],
        reply_stamp: timeNow,
        liked: [],
      });
      await reply.save();

      // 修改问的回复时间和回复数
      const question = await Questions.findOne({
        _id: req.body['question_id']
      }).exec();
      await Questions.findOneAndUpdate(
        question._id,
        {
          $set: {
            reply_stamp: timeNow
          },
          $inc: {
            reply_cnt: 1
          }
        }
      ).exec();

      res.json({
        code: 200,
        msg: '添加成功',
        reply_id: reply['_id']
      });
    } catch (err) {
      res.status(400).json({
        code: 400,
        msg: '添加失败',
        err: err,
      });
    }
  },
  // 删除回答
  async deleteReply (req, res) {
    try {
      const reply = await Replies.findOneAndDelete({
        _id: req.body['reply_id']
      }).exec();

      reply['image_list'].forEach(image => {
        fs.unlink('/images/' + image);
      });

      const question = await Questions.findOne({
        _id: reply.question_id
      }).exec();
      await Questions.findOneAndUpdate(
        question._id,
        {
          $inc: {
            reply_cnt: -1
          }
        }
      ).exec();

      res.json({
        code: 200,
        msg: '删除成功',
      });
    } catch (err) {
      res.status(400).json({
        code: 400,
        msg: '删除失败',
        err: err,
      });
    }
  },
  // 添加点赞
  async likeReply (req, res) {
    try {
      const replyQuery = Replies.find({
        _id: req.body['reply_id']
      });
      if (Boolean(parseInt(req.body['like_type'])) === true) {
        replyQuery.update({
          $addToSet: {
            liked: req.body['user_id']
          }
        });
      } else {
        replyQuery.update({
          $pull: {
            liked: req.body['user_id']
          }
        });
      }
      await replyQuery.exec();

      res.json({
        code: 200,
        like_state: Boolean(parseInt(req.body['like_type'])),
        msg: '修改成功'
      });
    } catch (err) {
      res.status(400).json({
        code: 400,
        msg: '修改失败',
        err: err,
      });
    }
  }
};

module.exports = repliesController;