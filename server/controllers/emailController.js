// 用于实现向用户邮箱发送验证码
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const {Users, Emails } = require('../model');

// 验证码过期时间(min)
const codeValidTime = 5;

// 创建一个SMTP客户端对象
const transporter = nodemailer.createTransport(smtpTransport({
  host: 'smtp.163.com', // 主机
  secure: true, // 使用 SSL
  port: 465, // SMTP 端口
  auth: {
    user: 'XuanKe_Pro@163.com', // 账号
    pass: 'CVOHMMKINQURPAOG' // SMTP授权码 课内仓库，先不考虑push时隐藏
  }
}));

// 生成随机六位验证码
const randomCode = () => {
  let code = '';
  for (let i = 0; i < 6; i++) {
    code += Math.floor(Math.random() * 10);
  }
  return code;
};

// 邮箱正则 - 改为同济邮箱
const emailReg =  /^[a-zA-Z0-9]+@tongji.edu.cn$/;
// const emailReg =  /^\w+((.\w+)|(-\w+))@[A-Za-z0-9]+((.|-)[A-Za-z0-9]+).[A-Za-z0-9]+$/;

// 注册时处理发送邮件请求
const sendEmail = async (req, res) => {
  const { email } = req.body;
  if (!emailReg.test(email)) {
    res.json({
      code: 400,
      msg: '邮箱格式不正确',
    });
    return;
  }
  try{
    // 查询邮箱是否已经存在
    const ret_email = await Emails.findOne({ email }).exec();

      if (ret_email) {
        res.json({
          code: 400,
          msg: '验证码已发送，请稍后重试',
        });
      } 
      else {
        // 创建一个验证码
        const code = randomCode();
        // 发送邮件
        transporter.sendMail({
          from: 'XuanKe_Pro@163.com',
          to: email,
          subject: '邮箱验证码',
          html: `
          <p>你好！</p>
          <p>您正在注册XuanKe Pro账号</p>
          <p>你的验证码是：<strong style="color: #ff4e2a;">${code}</strong></p>
          <p>***该验证码5分钟内有效***</p>` // html 内容
        }, 
        () => {
            // 将验证码保存到数据库
            const newEmail = new Emails({
              email,
              code,
              time: Date.now()
            });
            newEmail.save();
            
            res.json({
              code: 200,
              msg: '发送成功',
            });
            // 一分钟后验证码失效
            setTimeout(() => {
              Emails.findOneAndRemove({ email }, (err, email) => {});
            }, 60 * 1000 * codeValidTime);
          }
        
        );
      };
  }catch(err){
    res.json({
      code: 500,
      msg: '服务器错误',
    });
  }
};

// 忘记密码处理发送邮件请求
const forgetEmail = async (req, res) => {
  const { email } = req.body;
  if (!emailReg.test(email)) {
    res.json({
      code: 400,
      msg: '邮箱格式不正确',
    });
    return;
  }
  try{
    // 查询邮箱是否已经存在
    const user = await Users.findOne({ email: email }).exec();
    // const retEmail = await Emails.findOne({ email }).exec();
      if (user) {
        // 创建一个验证码
        const code = randomCode();
        // 发送邮件
        transporter.sendMail({
          from: 'XuanKe_Pro@163.com',
          to: email,
          subject: '邮箱验证码',
          html: `
          <p>你好！</p>
          <p>您正在修改XuanKe Pro账号密码</p>
          <p>你的验证码是：<strong style="color: #ff4e2a;">${code}</strong></p>
          <p>***该验证码5分钟内有效***</p>` // html 内容
        }, 
        () => {
            // 将验证码保存到数据库
            const newEmail = new Emails({
              email,
              code,
              time: Date.now()
            });
            newEmail.save();
            res.json({
              code: 200,
              msg: '发送成功',
            });
            // 一分钟后验证码失效
            setTimeout(() => {
              Emails.findOneAndRemove({ email }, (err, email) => {});
            }, 1000 * 60 * codeValidTime);
        }
        );
      }
      else  {
        res.json({
          code: 400,
          msg: '邮箱未注册，请重新输入',
        });
      } ;
  }catch(err){
    res.json({
      code: 500,
      msg: '服务器错误',
    });
  }
};

// 查询验证码是否正确
const checkCode = async (req, res) => {
  const { email, emailcode } = req.query;
  if (!emailReg.test(email)) {
    res.json({
      code: 400,
      msg: '邮箱格式不正确',
    });
    return;
  }
  try{
  // 查询验证码是否存在
  const retEmail = await Emails.find({ email }).exec();
    if (!retEmail) {
      res.json({
        code: 400,
        msg: '数据库中未找到关于该邮箱的记录',
      });
    } 
    else {
      for(var i = 0; i < retEmail.length; i++)
      {
        var tmp = retEmail[i].code;
        if (emailcode == retEmail[i].code)
        {
          res.json({
            code: 200,
            msg: '验证码正确',
          });
          return;
        }
      }
      //console.log(email);
      res.json({
        code: 400,
        msg: '验证码不正确',
      });
    };
}catch(err){

  res.json({
    code: 500,
    msg: '服务器错误',
  });
};
};

module.exports = { sendEmail, checkCode, forgetEmail};