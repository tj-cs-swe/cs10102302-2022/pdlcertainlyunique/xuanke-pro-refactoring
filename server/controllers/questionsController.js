const { query } = require('express');
const Model = require('../model');
const { Replies, Questions } = Model;

const fs = require('fs');

const questionsController = {
  // 获取问题
  async getQuestion(req, res) {
    try{

    
    const quesQuery = Questions.find({
      course_id: req.query['course_id'],
    });
    if (req.query['idx_type'] == 0) {
      quesQuery.sort({ sticked: -1, reply_stamp: -1 });
    } else if (req.query['idx_type'] == 1) {
      quesQuery.sort({ sticked: -1, timestamp: -1 });
    } else if (req.query['idx_type'] == 2) {
      quesQuery.sort({ sticked: -1, timestamp: 1 });
    }
    // var mquery_count = mquery.clone();
    const counter = await quesQuery.clone().count();
    //需处理请求失败情况
    quesQuery.skip(req.query['idx_up']);
    quesQuery.limit(req.query['idx_low'] - req.query['idx_up']);

    const question = await quesQuery.exec();

    question_list = [];
    question.forEach(element => {
      let x = false;
      element['liked'].forEach(like => {
        if (req.query['user_id'] == like) {
          x = true;
        }
      });
      let qs = {
        question_id: element['_id'],
        question_head: element['head'],
        thumbnail: element['thumbnail'],
        user_id: element['user_id'],
        reply_timestamp: element['reply_stamp'],
        reply_count: element['reply_cnt'],
        like_count: element['liked'].length,
        like_state: x,
        stick_state: element['sticked'],
      };
      question_list.push(qs);
    });
    res.json({
      code: 200,
      msg: '查询成功',
      question_count: counter,
      list_len: question.length,
      question_list: question_list,
    });
    
  }catch(err){
    console.log(err);
    res.status(400).json({
      code: 400,
      msg: '查询失败',
      err: err,
    });
  }
  },
  // 添加问题
  async addQuestion(req, res) {
    try{
      const question = new Questions({
        course_id: req.body['course_id'],
        head: req.body['question_head'],
        content: req.body['question_content'],
        thumbnail: req.body['thumbnail'],
        user_id: req.body['user_id'],
        image_list: req.body['image_list'],
        timestamp: Date.parse(new Date()),
        reply_stamp: Date.parse(new Date()),
        reply_cnt: 0,
        liked: [],
        sticked: false,
      });
      await question.save();
        
      res.json({
        code: 200,
        msg: '添加成功',
        question_id: question['_id']
      });
        
  }catch(err){
    res.status(400).json({
      code: 400,
      msg: '添加失败',
      err: err,
    });
  }
  },
  // 删除问题
  async deleteQuestion(req, res) {
    try{
      const question = await Questions.findOneAndDelete({ _id: req.body['question_id'] }).exec();
        
      question.image_list.forEach(element => {
        fs.unlink('/images/'+element);
      });
      res.json({
        code: 200,
        msg: '删除成功',
        question: question
      });
  }catch(err){
    res.status(400).json({
      code: 400,
      msg: '删除失败',
      err: err,
    });
  }
  },
  // 添加点赞
  async likeQuestion(req, res) {
    try{
    const quesQuery = Questions.find({
      _id: req.body['question_id']
    });
    if (Boolean(parseInt(req.body['like_type'])) === true) {
      quesQuery.update({
        $addToSet: {
          liked: req.body['user_id']
        }
      });
    } else {
      quesQuery.update({
        $pull: {
          liked: req.body['user_id']
        }
      });
    }
    await quesQuery.exec();
      
    res.json({
      code: 200,
      like_state: Boolean(parseInt(req.body['like_type'])),
      msg: '修改成功'
    });
  
  }catch(err){
    res.status(400).json({
      code: 400,
      msg: '修改失败',
      err: err,
    });
  }
  },
  // 置顶问题
  stickQuestion(req, res) {
    try{
      const quesQuery = Questions.find({
        _id: req.body['question_id']
      });
      if (Boolean(parseInt(req.body['stick_type'])) === true) {
        quesQuery.update({
          $set: {
            sticked: 1
          },
        });
      } else {
        quesQuery.update({
          $set: {
            sticked: 0
          },
        });
      }
      quesQuery.exec()
        
      res.json({
        code: 200,
        stick_state: Boolean(parseInt(req.body['stick_type'])),
        msg: '置顶成功'
      });
      
    }catch(err){
      res.status(400).json({
        code: 400,
        msg: '置顶失败',
        err: err,
      });
    }
  },
  // 查询内容
  async searchContent(req, res) {
    try{
      sc = req.query['search_content'];
      const question = await Questions.find({
        course_id: req.query['course_id'],
        $or: [
          {
            content: {
              $regex: sc,
              $options: 'i'
            }
          },
          {
            head: {
              $regex: sc,
              $options: 'i'
            }
          }
        ]
      });
      const question_all = await Questions.find({
        course_id: req.query['course_id'],
      });
      rplq = Replies.find({
        content: {
          $regex: sc,
          $options: 'i',
        }
      });
      rplq.where('_id').in(question_all);
      const answer = await rplq.exec();
      let ql = [];
      answer.forEach((item) => {
        ql.push(item['question_id']);
      });
      question.forEach((item) => {
        ql.push(item['_id']);
      });
      const quesQuery =  Questions.find({});
      quesQuery.where('_id').in(ql);
      if (req.query['idx_type'] == 0) {
        quesQuery.sort({ reply_stamp: -1 });
      } else if (req.query['idx_type'] == 1) {
        quesQuery.sort({ timestamp: -1 });
      } else if (req.query['idx_type'] == 2) {
        quesQuery.sort({ timestamp: 1 });
      }
      quesQuery.skip(req.query['idx_up']);
      quesQuery.limit(req.query['idx_low'] - req.query['idx_up']);
      const ans = await quesQuery.exec();
      
      let question_list = [];
      ans.forEach(element => {
        let x = false;
        element['liked'].forEach(like => {
          if (req.query['user_id'] == like) {
            x = true;
          }
        });
        let qs = {
          question_id: element['_id'],
          question_head: element['head'],
          thumbnail: element['thumbnail'],
          user_id: element['user_id'],
          reply_timestamp: element['reply_stamp'],
          reply_count: element['reply_cnt'],
          like_count: element['liked'].length,
          like_state: x,
          stick_state: element['sticked'],
        };
        question_list.push(qs);
      });
      res.json({
        code: 200,
        msg: '查询成功',
        question_count: ql.length,
        list_len: question.length,
        question_list: question_list,
      });
      
    }catch(err){
      res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: err,
      });
    }
  }  
};

module.exports = questionsController;
