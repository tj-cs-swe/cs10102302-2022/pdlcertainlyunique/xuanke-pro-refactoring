const { query } = require('express');
const Model = require('../model');
const { Courseinfos } = Model;

const coursesinfoController = {
  // 获取课程信息
  async getCourseinfo(req, res) {
    var mquery = Courseinfos.findOne({ course_id: req.query['course_id'] });
    try {
      const courseInfo = await mquery.exec();
      if (courseInfo == null) {
        res.status(400).json({
          code: 400,
          msg: '查询失败',
          err: '没有该课程',
        });
      }
      else {
        res.json({
          code: 200,
          msg: courseInfo['msg'],
          courseTitle: courseInfo['courseTitle'],
          courseTime: courseInfo['courseTime'],
          credit: courseInfo['credit'],
          teacher: courseInfo['teacher'],
          assessment: courseInfo['assessment'],
          teacher_message: courseInfo['teacher_message'],
          hasMessage: Boolean(courseInfo['teacher_message'] != null),
          course_intro: courseInfo['course_intro'],
          outline: courseInfo['outline'],
          labels: courseInfo['labels'],
        });
      }
    } catch (err) {
      res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: err,
      });
    }
  },
  // 提交课程信息
  async updateCourseinfo(req, res) {
    var mquery = Courseinfos.findOne({ course_id: req.body['course_id'] });
    try {
      const courseInfo = await mquery.exec();
      if (courseInfo != null) {
        console.log(courseInfo);
        var mquery2 = Courseinfos.findOne({ course_id: req.body['course_id'] });
        mquery2.update({
          $set: {
            course_intro: req.body['course_intro'],
            teacher_message: req.body['teacher_message'],
            outline: req.body['outline'],
            labels: req.body['labels'],
          }
        });
        try {
          await mquery2.exec();
          res.json({
            code: 200,
            msg: '修改成功'
          });
        } catch (err) {
          res.status(400).json({
            code: 400,
            msg: '修改失败',
            err: err,
          });
        }
      } else {
        var newCourseInfo = new Courseinfos({
          course_id: req.body['course_id'],
          course_intro: req.body['course_intro'],
          teacher_message: req.body['teacher_message'],
          outline: req.body['outline'],
          labels: req.body['labels'],
        });
        try {
          await newCourseInfo.save();
          res.json({
            code: 200,
            msg: '添加成功',
            _id: newCourseInfo['_id']
          });
        } catch (err) {
          res.status(400).json({
            code: 400,
            msg: '添加失败',
            err: err,
          });
        }
      }
    } catch (err) {
      res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: err,
      });
    }
  }
};

module.exports = coursesinfoController;