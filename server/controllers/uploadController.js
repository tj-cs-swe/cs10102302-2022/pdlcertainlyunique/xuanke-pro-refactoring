const config = require('../config/config.js');

const uploadController = {
  uploadImage (req, res) {
    res.json({
      errno: 0,
      data: {
        url: `http://${config.upload.host}:${config.upload.port}/images/${req.file.filename}`,
      },
    });
  },
};
module.exports = uploadController;