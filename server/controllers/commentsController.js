const { query } = require('express');
const Model = require('../model');
const { Comments } = Model;

const SORT_BY_LIKE_COUNT = 0;
const SORT_BY_TIME = 1;

const coursesinfoController = {
  // 获取评论
  async getComment(req, res) {
    var mquery = Comments.find({ course_id: req.query['course_id'] });
    if (req.query['idx_type'] == SORT_BY_LIKE_COUNT) {
      mquery.sort({ like_count: -1 });
    } 
    else if (req.query['idx_type'] == SORT_BY_TIME) {
      mquery.sort({ time: -1 });
    }
    try {
      const comments = await mquery.exec();
      res.json({
        code: 200,
        msg: '查询成功',
        list_size: comments.length,
        comment_list: comments,
      });
    } catch (err) {
      res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: err,
      });
    }
  },
  // 添加评论
  async addComment(req, res) {
    var comment = new Comments({
      course_id: req.body['course_id'],
      user_id: req.body['user_id'],
      score: req.body['score'],
      content: req.body['content'],
      time: Date.parse(new Date()),
      like_count: 0,
      liked: []
    });
    try {
      await comment.save();
      res.json({
        code: 200,
        msg: '添加成功',
      });
    } catch (err) {
      res.status(400).json({
        code: 400,
        msg: '添加失败',
        err: err,
      });
    }
  },
  // 点赞评论
  async likeComment(req, res) {
    var mquery = Comments.find({
      _id: req.body['comment_id']
    });
    if (Boolean(parseInt(req.body['like_type'])) == true) {
      mquery.update({
        $push: {
          liked: req.body['user_id']
        },
        $inc: {
          like_count: 1
        },
      });
    } else {
      mquery.update({
        $pull: {
          liked: req.body['user_id']
        },
        $inc: {
          like_count: -1
        },
      });
    }
    try {
      await mquery.exec();
      res.json({
        code: 200,
        like_state: Boolean(parseInt(req.body['like_type'])),
        msg: '修改成功',
      });
    } catch (err) {
      res.status(400).json({
        code: 400,
        msg: '修改失败',
        err: err,
      });
    }
  },
  // 删除评论
  async deleteComment(req, res) {
    try {
      await Comments.findOneAndDelete({ _id: req.body['comment_id'] }).exec();
      res.json({
        code: 200,
        msg: '删除成功',
      });
    } catch (err) {
      res.status(400).json({
        code: 400,
        msg: '删除失败',
        err: err,
      });
    }
  },
};

module.exports = coursesinfoController;