const dotenv = require('dotenv');
dotenv.config('../.env');

module.exports = {
  db: {
    database: process.env.DB_NAME || 'xuanke-pro',
    host: process.env.HOST || 'localhost',
    port: process.env.DB_PORT || 27017,
  },
  cors: {
    origin: process.env.ORIGIN || 'http://localhost:8080',
  },
  authentication: {
    jwtSecret: process.env.JWT_SECRET || 'secret',
    sessionSecret: process.env.SESSION_SECRET || 'secret'
  },
  upload: {
    host: process.env.HOST || 'localhost',
    port: process.env.UPLOAD_PORT || 3000,
  }
};
