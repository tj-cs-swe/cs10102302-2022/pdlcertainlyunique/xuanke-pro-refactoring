const commentsController = require('../../controllers/commentsController');

module.exports = (router) => {
  // 课程评论
  router.get('/comment/get-comment', commentsController.getComment);
  router.post('/comment/like-comment', commentsController.likeComment);
  router.post('/comment/add-comment', commentsController.addComment);
  router.post('/comment/delete-comment', commentsController.deleteComment);
};