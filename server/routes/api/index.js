const express = require('express');
const router = express.Router();
const fs = require('fs');
const path = require('path');

fs
  .readdirSync(__dirname)
  .filter((file) =>
    file !== 'index.js'
  ) // 读取 api 中的 route 文件
  .forEach((file) => {
    require(path.join(__dirname, file))(router);
  });

module.exports = router;