const coursesController = require('../../controllers/coursesController');
const usersController = require('../../controllers/usersController');

module.exports = (router) => {
  // 课程相关
  router.get('/courses/get-courses', coursesController.getCourses);
  router.get('/courses/get-timetable', usersController.getTimetableByEmail);
};