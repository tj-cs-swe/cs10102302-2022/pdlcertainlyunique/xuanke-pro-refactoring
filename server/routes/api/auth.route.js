const accountController = require('../../controllers/accountController');
const { sendEmail, checkCode, forgetEmail} = require('../../controllers/emailController');

module.exports = (router) => {
  // 注册相关
  router.get('/auth/check-emailcode', checkCode);
  router.post('/auth/send-emailcode', sendEmail);

  // 账号相关
  router.get('/login/user', accountController.login);
  router.post('/auth/user', accountController.add);
  router.post('/auth/resest-password', accountController.resetPassword);
  router.post('/auth/change-password', accountController.changePassword);

  // 忘记密码
  router.post('/auth/forget-emailcode', forgetEmail);
};