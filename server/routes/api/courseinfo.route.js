const coursesinfoController = require('../../controllers/courseinfosController');

module.exports = (router) => {
  // 课程信息
  router.get('/courseinfo/get-courseinfo', coursesinfoController.getCourseinfo);
  router.post('/courseinfo/update-courseinfo', coursesinfoController.updateCourseinfo);
};