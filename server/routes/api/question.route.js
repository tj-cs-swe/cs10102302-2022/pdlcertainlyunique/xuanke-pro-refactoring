const questionsController = require('../../controllers/questionsController');

module.exports = (router) => {
  // 问题相关
  router.get('/forum/get-question', questionsController.getQuestion);
  router.get('/forum/search-content', questionsController.searchContent);
  router.post('/forum/add-question', questionsController.addQuestion);
  router.post('/forum/delete-question', questionsController.deleteQuestion);
  router.post('/forum/like-question', questionsController.likeQuestion);
  router.post('/forum/stick-question', questionsController.stickQuestion);
};