const repliesController = require('../../controllers/repliesController');

module.exports = (router) => {
  // 回答相关
  router.get('/forum/get-reply', repliesController.getReply);
  router.post('/forum/add-reply', repliesController.addReply);
  router.post('/forum/delete-reply', repliesController.deleteReply);
  router.post('/forum/like-reply', repliesController.likeReply);
};