const uploadController = require('../../controllers/uploadController');
const multer = require('multer');
var upload = multer({ dest:'images/' });

module.exports = (router) => {
  // 上传文件
  router.post('/upload/upload-image', upload.single('images'), uploadController.uploadImage);
};





