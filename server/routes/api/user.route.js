const usersController = require('../../controllers/usersController');

module.exports = (router) => {
  // 用户信息相关
  router.get('/user/info', usersController.getUserinfo);
  router.post('/user/post-info', usersController.addUserinfo);
  router.get('/user/collections', usersController.getCollection);
  router.post('/user/collections', usersController.addCollection);
  router.delete('/user/collections', usersController.deleteCollection);
  router.post('/user/courses', usersController.addTimetable);
  router.get('/user/courses', usersController.getTimetableByEmail);
  router.post('/user/avatar', usersController.addAvatar);
  router.delete('/user/courses', usersController.deleteTimetable);

  // test
  router.get('/user/all', usersController.all);
};