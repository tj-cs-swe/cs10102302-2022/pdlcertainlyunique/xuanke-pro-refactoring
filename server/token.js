const jwt = require('jsonwebtoken');

// 获取 config，其中有 json web token 的 serect
const config = require('./config/config.js');

exports.setToken = (email) => {
  return new Promise((resolve, reject) => {
    jwt.sign({ email }, config.authentication.jwtSecret, 
      { expiresIn: '1h' }, (err, token) => {
        if (err) {
          reject(err);
        } else {
          resolve(token);
        }
      });
  });
};

exports.verifyToken = (token) => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, config.authentication.jwtSecret, (err, decoded) => {
      if (err) {
        reject(err);
      } else {
        resolve(decoded);
      }
    });
  });
};