const session = require('express-session');
const config = require('./config/config.js');

module.exports = (app) => {
  const _session = session({
    secret: `${config.authentication.sessionSecret}`,
    resave: false,
    saveUninitialized: true,
    cookie: {
      maxAge: 1000 * 60 * 60 * 24 * 7,
    },
  });

  app.use(_session);
};